import React from "react";
import './menuitem.css';

export function MenuItem({img, title}) {
    return (
        <div className="div-menu-item">
            <img src={img} alt="item"></img>
            <div className="text-menu-item">{title}</div>
        </div>
    );
}
