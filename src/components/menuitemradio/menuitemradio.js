import React from "react";
import './menuitemradio.css';
import iconLightMode from '../../image/Icon-lightmode.png';

export function MenuItemRadio() {
    return (
        <div className="div-menuitem-radio">
            <img src={iconLightMode} alt="logo"/>
            <div className="text-menu-item-radio">Light mode</div>
            <label className="div-container">
                <input type="checkbox"/>
                <span className="checkmark"></span>
            </label>
        </div>
    );
}