import React from "react";
import { MenuItem } from "../menuitem/menuitem";
import { MenuItemRadio } from "../menuitemradio/menuitemradio";
import './sidemenu.css';

import iconSearch from '../../image/Icon-search.png';
import iconDashboard from '../../image/Icon-dashboard.png';
import iconRevenue from '../../image/Icon-revenue.png';
import iconNotif from '../../image/Icon-notif.png';
import iconAnalytics from '../../image/Icon-analytics.png';
import iconInventory from '../../image/Icon-inventory.png';
import iconLogout from '../../image/Icon-logout.png';

export function SideMenu() {
    return (
        <div className="side-bar-main">
            <div className="div-user-info text-light">
                <div className="div-ava">AF</div>
                <div className="div-username">AnimatedFred</div>
                <div className="div-usermail">animated@demo.com</div>
                <div className="div-arrow"></div>
            </div>
            <ul>
                <MenuItem img={iconSearch} title='Search'/>
                <MenuItem img={iconDashboard} title='Dashboard'/>
                <MenuItem img={iconRevenue} title='Revenue'/>
                <MenuItem img={iconNotif} title='Notification'/>
                <MenuItem img={iconAnalytics} title='Analytics'/>
                <MenuItem img={iconInventory} title='Inventory'/>
            </ul>
            <ul>
                <MenuItem img={iconLogout} title="Logout"/>
                <MenuItemRadio/>
            </ul>
        </div>
    );
}