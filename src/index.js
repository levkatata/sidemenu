import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { SideMenu } from './components/sidemenu/sidemenu';

ReactDOM.render(<SideMenu></SideMenu>, document.getElementById("main"));
